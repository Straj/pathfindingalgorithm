import java.util.Comparator;
import java.util.Iterator;
import java.util.PriorityQueue;
import java.util.concurrent.TimeUnit;

/**
 * Created by Влад on 06.12.2014.
 */
public class JPS {

    int xFinalPoint,
        yFinalPoint;

    int [][] grid;
    char [][] gridOut;

    String sPath ="";
    String sLabyrinth="";

    long workTime;
    long startTime;

    //Элемент с наименьшей стоимостью всегда первый
    private PriorityQueue <Cell> openList = new PriorityQueue<Cell>(1, new Comparator<Cell>() {
        @Override
        public int compare(Cell a, Cell b) {
            return (int) (a.getF() - b.getF());
        }
    });



    JPS (int xStartPoint, int yStartPoint, int xFinalPoint, int yFinalPoint, int [][] grid) {
        startTime = System.currentTimeMillis();

        Cell startPoint = new Cell (xStartPoint, yStartPoint);
        openList.add(startPoint);

        this.xFinalPoint = xFinalPoint;
        this.yFinalPoint = yFinalPoint;

        this.grid = grid;

        creatingGridOut();
    }



    //Метод начальных инструкций, основного тика и условий выхода
    public Cell search () {
        while (!openList.isEmpty()) {

            Iterator<Cell> iter = openList.iterator();

            System.out.println("Printing open list");
            while (iter.hasNext()) {
                System.out.println(iter.next());
            }

            Cell currentCell = openList.poll();
            System.out.println("Current cell is" +currentCell);
            if (currentCell.getY()==yFinalPoint && currentCell.getX()==xFinalPoint) {
                System.out.println("Path was successfully founded");
                sPath(currentCell);
                sLayrinthCreate();
                return currentCell;
            }
            getSuccessorss(currentCell);

        }

        return new Cell(-1, -1);

    }

    //В этом методе мы получаем точки приемники для соседей текущей клетки
    private void getSuccessorss(Cell currentCell) {
        int [][] neighbors= getPrunedNeighbors(currentCell);

        int coordinates[][] = new int[2][1];
        coordinates[0][0]=0;
        coordinates[1][0]=0;

        for (int i=0; i<neighbors[0].length; i++) {
            System.out.print("x " +neighbors[0][i] + "y " +neighbors[1][i] +"\n");
        }


        for (int i=0; i<neighbors[0].length; i++) {



            System.out.println("ckecking neightbour of current cell" +neighbors[0][i] +" " +neighbors[1][i]);
            if (neighbors[0][i]!=0 && neighbors[1][i]!=0){
                coordinates = jump(neighbors[0][i], neighbors[1][i], currentCell.getX(), currentCell.getY());
            }

            if (coordinates[0][0]!=-1 && (coordinates[1][0]!=0 || coordinates[0][0]!=0)) {
                Cell successorCell = new Cell(coordinates[0][0], coordinates[1][0]);
                System.out.println("New successorCell is" +coordinates[0][0] +" ; " +coordinates[1][0]);

                if (openList.contains(successorCell)) {

                    Iterator<Cell> itr = openList.iterator();
                    Cell temp;

                    while (itr.hasNext()) {
                        temp=itr.next();
                        if (temp==successorCell) {
                            setFGH(successorCell, temp, currentCell);
                            System.out.println("F of " +currentCell + " =" +currentCell.getF());
                            successorCell.setParent(currentCell);
                            openList.remove(temp);
                            openList.add(successorCell);
                        }
                    }
                } else {
                    setFGH(successorCell, null, currentCell);
                    System.out.println("F of " +currentCell + " =" +currentCell.getF());
                    successorCell.setParent(currentCell);
                    openList.add(successorCell);
                }
            }
        }
    }


    //Пытаемся зделать прыжок. Если можно - текущая точка - годная, и мы добавим ее в очередь с приоритетом
    private int [][] jump (int x, int y, int pX, int pY) {
        int [][] coordinates = {{-1}, {-1}};

        //Получаем векторы направлений движения. Единица нужна для того, что бы не было деления на 0.
        int dx = (x-pX)/Math.max(Math.abs(x-pX),1);
        int dy = (y-pY)/Math.max(Math.abs(y-pY),1);
        System.out.println("dx "+dx +"dy" +dy);

        //Если случится, что мы получили непроходимую клетку - не начинать прыжки для этой точки
        if (!walkable(x, y)) {
            System.out.println("not walkable");
            return coordinates;

        }

        if (x==xFinalPoint && y==yFinalPoint) {
            coordinateToArray(coordinates, 0, x, y);
            System.out.println("end cell");
            return coordinates;
        }

        //Если есть вынужденные соседи под боком - то клетка нам подходит
        if (dx!=0 && dy!=0) {
            System.out.println("Cell [" +(x+dx) +"; "+(y-dy) +"] is " +walkable(x+dx, y-dy) +"here neighbor is" +walkable(x, y-dy));
            System.out.println("Cell [" +x +"; "+(y-dy) +"] is " +walkable(x-dx, y+dy) +"here neighbor is" +walkable(x-dx, y) );
            if (walkable(x-dx, y+dy) && !walkable(x-dx, y) || walkable(x+dx, y-dy) && !walkable(x,y-dy)) {
                coordinateToArray(coordinates, 0, x, y);
                System.out.println("diagonal moving, forced neighbors exists");
                return coordinates;
            }

        } else {
            if (dy==0) {
                //Имеем дело с горизонтальным движением

                if (walkable(x+dx, y)&& walkable(x+dx, y-1) && !walkable(x, y-1) ||
                        walkable(x+dx,y) && walkable(x+dx, y+1) && !walkable(x, y+1)) {
                    coordinateToArray(coordinates, 0, x, y);
                    System.out.println("horisontal moving");
                    return coordinates;
                }
            }
            else {
                //Вертикальное
                System.out.println("testing");
                System.out.println(walkable(x, y+dy));
                System.out.println(walkable(x-1, y+dy));
                System.out.println(!walkable(x-1, y));
                if (walkable(x, y+dy) && walkable(x-1, y+dy) && !walkable(x-1, y) ||
                        walkable(x, y+dy) && walkable(x+1, y+dy) && !walkable(x+1, y)) {
                    coordinateToArray(coordinates, 0, x, y);
                    System.out.println("vertical moving");
                    return coordinates;
                }
            }
        }

        //Окей, мы не нашли принужденных соседей у данной точки. Тогда пробуем двигаться дальше

        if (Math.abs(dx)==1 && Math.abs(dy)==1) {
            //Для диагольного перемещения, мы сначала выполняем перемещение в направлении только dx и только dy, и если
            //не было найдено вынужденных соседей, движемся на одну клетку по диагонали

            int jx =jump (x+dx, y, x, y)[0][0];
            int jy =jump(x, y+dy, x, y)[1][0];

            //Если в процессе движения по горизонтали/вертикали, мы таки что-то нашли
            if (jx!=-1 || jy!=-1) {
                coordinateToArray(coordinates, 0, x, y);
                return coordinates;
            }
        }

        //Это прыжки для вертикальных/горизонтальных клеток, они нужны, если рядом с первоначальным соседом
        // не было найдено вынужденных соседей - тогда мы движемся дальше
        if (walkable(x+dx, y) || walkable(x, y+dy)) {
            return jump(x+dx, y+dy, x,y);
        } else {
            return coordinates;
        }
    }

    //Отсечь все неоптимальные клетки
    private int[][] getPrunedNeighbors (Cell currentCell) {

        int x = currentCell.getX(),
            y = currentCell.getY();

        int counter = 0;

        if (currentCell.getParent()!=null) {
            Cell parent = currentCell.getParent();
            int pX = parent.getX(),
                pY = parent.getY();

            //Получаем векторы направлений движения. Единица нужна для того, что бы не было деления на 0.
            int dx = (x-pX)/Math.max(Math.abs(x-pX),1);
            int dy = (y-pY)/Math.max(Math.abs(y-pY),1);

            //Массив соседей. 4 соседа - максимум, после отсечения.
            int [][] neighbors = new int [2][4];


            //Меняются два вектора - движение диагональное
            if (dx!= 0 && dy!=0) {
                if (walkable(x, y+dy)) {
                    neighbors=coordinateToArray(neighbors, counter, x, y+dy);
                    counter++;
                }

                if (walkable(x+dx, y)) {
                    neighbors=coordinateToArray(neighbors, counter, x + dx, y);
                    counter++;

                    for (int i=0; i<neighbors[0].length; i++) {
                        System.out.print("x " +neighbors[0][i] + "y " +neighbors[1][i] +"\n");
                    }
                    System.out.println(counter);
                }

                if ((walkable(x, y+dy) || walkable(x+dx, y)) && walkable(x+dx, y+dy)) {
                    neighbors=coordinateToArray(neighbors, counter, x+dx, y+dy);
                    counter++;
                }

                if (!walkable(x-dx, y) && walkable(x-dx,y+dy) && walkable(x, y+dy)) {
                    neighbors=coordinateToArray(neighbors, counter, x-dx, y+dy);
                    counter++;
                }

                if (!walkable(x, y-dy) && walkable(x+dx,y) && walkable(x+dx, y-dy)) {
                    neighbors=coordinateToArray(neighbors, counter, x+dx, y-dy);
                    counter++;
                    for (int i=0; i<neighbors[0].length; i++) {
                        System.out.print("x " +neighbors[0][i] + "y " +neighbors[1][i] +"\n");

                    }
                    System.out.println(counter);
                }
            } else {
                if (dy==0) {
                    //Движение горизонтальное
                    if (walkable(x+dx, y)) {
                        neighbors=coordinateToArray(neighbors, counter, x+dx, y);
                        counter++;
                    }

                    if (!walkable(x, y-1) && walkable(x+dx, y-1)) {
                        neighbors=coordinateToArray(neighbors, counter, x+dx, y-1);
                        counter++;
                    }

                    if (!walkable(x, y+1) && walkable(x+dx, y+1)) {
                        neighbors=coordinateToArray(neighbors, counter, x+dx, y+1);
                        counter++;
                    }
                } else {
                    //Остается только вертикаль. Ну я надеюсь
                    if (walkable(x, y+dy)) {
                        neighbors=coordinateToArray(neighbors, counter, x, y+dy);
                        counter++;
                    }

                    if (!walkable(x-1, y) && walkable(x-1, y+dy)) {
                        neighbors=coordinateToArray(neighbors, counter, x-1, y+dy);
                        counter++;
                    }

                    if (!walkable(x+1, y) && walkable(x+1, y+dy)) {
                        neighbors=coordinateToArray(neighbors, counter, x+1, y+dy);
                        counter++;
                    }
                }
            }
            return neighbors;

        } else {
            //Мы имеем дело со стартовой точкой. У нее рассматриваются все соседи

            //Массив соседей. 8 соседей - максимум
            int [][] neighbors = new int [2][8];

            for (int j=y-1; j<=y+1; j++) {
                for (int i=x-1; i<=x+1; i++) {
                    if (walkable(i,j) && !(Math.abs(j-y) + Math.abs(i-x)==0)) {
                        coordinateToArray(neighbors, counter, i, j);
                        neighbors [0][counter] = i;
                        neighbors [1][counter] = j;
                        counter++;
                    }
                }
            }
            return neighbors;
        }
    }

    //Вспомагательный метод.
    int [][] coordinateToArray(int[][] mass, int counter, int x, int y) {
        mass[0][counter]=x;
        mass[1][counter]=y;
        System.out.println("counter in method "+counter);
        return  mass;
    }

    //Все просто. Тут идет проверка на проходимость
    private boolean walkable (int x, int y) {
        if (grid[y][x]==1) {return false;}
        else {return true;}
    }

    //Вспомогательный методо для подсчета G
    private int gCounting (Cell currentCell, Cell successor) {

        int lenX = Math.abs(currentCell.getX()-successor.getX());
        int lenY = Math.abs(currentCell.getY()-successor.getY());
        int dif;

        if (lenX!=0 && lenY!=0) {
            dif=lenX*14;
        } else {
            dif=(lenX==0)? lenY*10 : lenX*10;
        }
       return currentCell.getG() +dif;
    }


    //Вспомогательный методо для подсчета H
    private void hCounting(Cell successor) {

        int lenX = Math.abs(successor.getX()-xFinalPoint);
        int lenY = Math.abs(successor.getY()-yFinalPoint);

        successor.setH((lenX+lenY)*10);
    }

    //Метод, который выведет весь путь, который мы прошли: от конечной точки и до начальной


    String labyrinthOut;

    public String printGridOut() {
        for (int i=0; i<gridOut.length; i++) {
            for (int j=0; j<gridOut[0].length; j++) {
                if (labyrinthOut==null) {
                    labyrinthOut=gridOut[i][j]+"";
                    continue;
                }
                labyrinthOut=labyrinthOut+gridOut[i][j]+"";
            }
            labyrinthOut=labyrinthOut+"\n";
        }
        return labyrinthOut;
    }

    public String sPath(Cell cell) {
        sPath +=cell +"\n";
        gridOut[cell.getY()][cell.getX()]='*';
        if (cell.getParent()!=null) {
            sPath(cell.getParent());
        }
        return sPath;
    }

    public String sLayrinthCreate() {
        for (int i=0; i< grid.length; i++) {
            for (int j=0; j<grid[0].length; j++) {

                if (gridOut[i][j]=='*') {
                    sLabyrinth+='+';
                    continue;
                }

                if (grid[i][j]==1) {
                    sLabyrinth+="#";
                } else {
                    sLabyrinth+=grid[i][j];
                }
            }
            sLabyrinth+="\n";
        }
        return sLabyrinth;

    }

    public void creatingGridOut() {
        gridOut = new char[grid.length][grid[0].length];
        for (int i=0; i<grid.length; i++) {
            for (int j=0; j<grid[0].length; j++) {
                gridOut[i][j]=(char)grid[i][j];
            }
        }
    }

    public String timeSpend() {
        long timeSpent = (System.currentTimeMillis() - startTime);
        return String.valueOf(timeSpent);

    }

    //Этот метод подсчитает F G H и инициализирует ими соответсвующие поля нового приемника
    private void setFGH(Cell successor, Cell oldCell, Cell currentCell) {

        hCounting(successor);

        int nG=gCounting(currentCell, successor);
        if (oldCell!=null) {
            if (oldCell.getG()> nG) {
                successor.setG(nG);
            }else {
                successor.setG(nG);
            }
        } else {
            successor.setG(nG);
        }

        int f = successor.getG()+successor.getH();
        successor.setF(f);
    }
}
