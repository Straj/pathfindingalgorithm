/**
 * Created by Влад on 13.12.2014.
 */


import java.util.*;

/**
 * Created by Влад on 16.11.2014.
 */

public class AStar {

    private int grid[][];
    private char gridOut[][];
    String sPath = "";
    String sLabyrinth ="";
    private int xFinalPoint, yFinalPoint;

    long workTime;
    long startTime;

    private Cell currentCell;
    private HashSet<Cell> openList;
    //private Cell checkingCell;

    AStar(int[][] grid, int xStartPoint, int yStartPoint, int xFinalPoint, int yFinalPoint) {
        startTime = System.currentTimeMillis();

        this.grid = grid;
        openList = new HashSet<Cell>();

        currentCell = new Cell(xStartPoint, yStartPoint);
        currentCell.setG(0);
        currentCell.setH(0);
        currentCell.setF(0);
        currentCell.setStatus(false);

        this.xFinalPoint = xFinalPoint;
        this.yFinalPoint = yFinalPoint;

        creatingGridOut();

        openList.add(currentCell);
    }

    // Старт Алгоритма
    public void start() {
        //while (currentCell!=finalCell) {

        Cell finalCell = pathFinding(currentCell);
        sPath = sPath(finalCell);

        for (int i=0; i<gridOut.length; i++) {
            for (int j=0; j<gridOut[0].length; j++) {
                System.out.print(gridOut[i][j]);
            }
            System.out.println();
        }
        sLabyrinth = sLayrinthCreate(finalCell);

        //}
    }

    private Cell pathFinding(Cell currentCell) {
        Cell checkingCell;


        //Выход, если мы нашли конечную точку
        if (currentCell.getX()==xFinalPoint && currentCell.getY()==yFinalPoint) {
            System.out.print("Suck");
            return currentCell;
        }

        System.out.println("---------------------------");
        System.out.println("-----------NewStep---------");
        System.out.println("---CurrentCell" +currentCell +"---");
        System.out.println("---Here G is" +currentCell.getG()+"---");
        System.out.println("");

        //Пробегаемся по всем клеткам в радиусе >=1 от нашей

        for (int j = currentCell.getY() - 1; j <= currentCell.getY() + 1; j++) {
            for (int i = currentCell.getX() - 1; i <= currentCell.getX() + 1; i++) {

                checkingCell = new Cell(i, j);
                System.out.println("Checking cell is " +checkingCell);

                if (grid[j][i] == 1) {
                    System.out.println("It's a wall");
                    System.out.println();
                    continue;
                }

                //Проверяем содержится ли наша клетка во списке
                if (openList.contains(checkingCell)) {
                    System.out.println("Cell is already in the list");
                    System.out.println();
                    gCompare(checkingCell);


                } else {
                    System.out.println("How, it's a new Cell!");
                    System.out.println();

                    checkingCell.setParent(currentCell);

                    gCounting(checkingCell, currentCell);
                    System.out.println("Here g =" +checkingCell.getG());

                    checkingCell.setH(hCounting(checkingCell));

                    checkingCell.setF(fCounting(checkingCell));
                    checkingCell.setStatus(true);

                    System.out.println("Adding cell" + checkingCell);
                    openList.add(checkingCell);
                }
            }
        }
        currentCell.setStatus(false);
        currentCell = getMinCostElement();
        System.out.println("The element with min cost is " +currentCell);
        System.out.println("GGGG" +currentCell.getG());

        return pathFinding(currentCell);
    }

    //Считаем G
    public void gCounting (Cell checkingCell, Cell currentCell) {

        System.out.println("CurrentCell g is" +currentCell.getG());
        if (Math.abs(currentCell.getX()-checkingCell.getX())==1 && Math.abs(currentCell.getY()-checkingCell.getY())==1) {
            checkingCell.setG(currentCell.getG()+14);
            //g = this.currentCell.getG() +14;
            //System.out.println("Cur" +currentCell);
            System.out.println("G of" + checkingCell.getG());
            //return g;

        } else{
            checkingCell.setG(currentCell.getG()+10);
            // g = this.currentCell.getG() +10;
            //System.out.println("Cur" +currentCell);
            System.out.println("G of" + checkingCell.getG());
            //return checkingCell;
        }
    }

    //Выбираем путь G что покороче
    private void gCompare (Cell checkingCell) {
        int gOld = checkingCell.getG();
        int gInc;

        if (Math.abs(currentCell.getX()-checkingCell.getX())==1 && Math.abs(currentCell.getY()-checkingCell.getY())==1) {
            gInc=14;
        } else{
            gInc =10;
        }

        int gNew=currentCell.getG()+gInc;

        if (gNew<gOld) {
            System.out.println("CHANGING, CHANGING");
            checkingCell.setG(gNew);
            checkingCell.setF(fCounting(checkingCell));
            checkingCell.setParent(currentCell);
        }
        //return checkingCell;
    }
    //Считаем H
    private int hCounting (Cell checkingCell) {
        int h = Math.abs(checkingCell.getX()-xFinalPoint)+ Math.abs(checkingCell.getY()-yFinalPoint);
        return h*10;
    }

    //Считаем F
    private int fCounting (Cell checkingCell) {
        int f = checkingCell.getG()+checkingCell.getH();
        return f;
    }

    public Cell getMinCostElement () {

        Cell testCell =new Cell();
        Cell testCell2 = new Cell();
        boolean t;
        System.out.println(openList);
        Iterator <Cell> itr = openList.iterator();

        while (itr.hasNext()) {
            testCell=itr.next();
            if (testCell.getStatus()) {
                System.out.println("First not close cell is" + testCell +testCell.getF() + " " +testCell.getStatus());
                break;
            }
        }

        Iterator <Cell> itr2 = openList.iterator();

        while(itr2.hasNext()) {
            testCell2 =itr2.next();

            if (testCell2.getStatus()==true && testCell.getF()>testCell2.getF()){//currentCell.getF()>testCell.getF()) {
                testCell = testCell2;
                System.out.println("Element what pass" + testCell);
                //currentCell = testCell;
            }
        }
        return testCell;
        //System.out.println("GUESS WHAT?" + currentCell +"\n" +"\n");
    }

    public void creatingGridOut() {
        gridOut = new char [grid.length][grid[0].length];
        for (int i=0; i<grid.length; i++) {
            for (int j=0; j<grid[0].length; j++) {
                if (grid[i][j]==0) {
                    gridOut[i][j]='0';
                } else
                if (grid[i][j]==1) {
                    gridOut[i][j]='#';
                }
            }
        }
    }

    public String sPath(Cell cell) {
        sPath +=cell +"\n";
        gridOut[cell.getY()][cell.getX()]='*';
        if (cell.getParent()!=null) {
            sPath(cell.getParent());
        }
        return sPath;
    }

    public String sLayrinthCreate(Cell cell) {
        for (int i=0; i< grid.length; i++) {
            for (int j=0; j<grid[0].length; j++) {

                if (gridOut[i][j]=='*') {
                    sLabyrinth+='+';
                    continue;
                }

                if (grid[i][j]==1) {
                    sLabyrinth+="#";
                } else {
                    sLabyrinth+=grid[i][j];
                }
            }
            sLabyrinth+="\n";
        }
        return sLabyrinth;

    }

    public String timeSpend() {
        long timeSpent = (System.currentTimeMillis() - startTime);
        return String.valueOf(timeSpent);

    }



}
