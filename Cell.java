

/**
 * Created by Влад on 16.11.2014.
 */
public class Cell {
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Cell)) return false;

        Cell cell = (Cell) o;

        if (x != cell.x) return false;
        if (y != cell.y) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        return result;
    }

    //Статус клетки.
    private boolean status;

    // Координаты клетки
    private int x;
    private int y;

    //Оценки стоимости клетки F= G+H;
    private int f;
    private int g;
    private int h;

    //Указатель на предыдую клетку
    private Cell parent;

    Cell () {
    }

    Cell (int x, int y) {
        this.x=x;
        this.y=y;
    }

    public String toString() {
        return "[ " + x + " ; " + y + "]";
    }

    public void setParent (Cell cell) {
        parent = cell;
    }

    public Cell getParent () {
        return parent;
    }

    public int getX () {
        return x;
    }

    public int getY () {
        return y;
    }

    public void setY (int y) {
        this.y=y;
    }

    public void setX (int x) {
        this.x=x;
    }

    public void setF(int f) {
        this.f=f;
    }

    public void setG(int g) {
        this.g =g;
    }

    public void setH(int h) {
        this.h= h;
    }

    public int getG() {
        return g;
    }

    public int getH() {return h;}

    public boolean getStatus() {return status;}

    public void setStatus(boolean status) {this.status = status;}

    public int getF() {
        return f;
    }
}
